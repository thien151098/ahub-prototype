import styled from "styled-components";

export const HeaderWrapper = styled.div`
  .ant-layout-header {
    padding: 0 24px;
    padding-left: 24px;
    display: flex;
    align-items: center;
    background-color: #fff;
    box-shadow: 0 0 4px 4px #00000014;
    z-index: 10;
    justify-content: space-between;
  }
`;

export const StyledMenuItem = styled.div`
  display: flex;
  align-items: center;
  width: 100%;

  .ant-dropdown-menu-title-content {
    display: block;
  }
`;
export const MenuWrapper = styled.div`
  .ant-menu-item:has(.active-link) {
    color: #da271c;
    background-color: transparent;

    &::after {
      border-width: 2px;
      border-bottom-color: #da271c;
    }

    &:hover {
      color: #da271c !important;
    }
  }

  .ant-menu-item-selected {
    color: rgb(218, 39, 28) !important;
  }

  .ant-menu-item:hover::after,
  .ant-menu-item-selected::after {
    border-bottom-color: rgb(218, 39, 28) !important;
  }
`;
