import { useState } from "react";
import {
  Layout,
  Menu,
  Dropdown,
  Space,
  Typography,
  Avatar,
  Badge,
  Row,
  Button,
  message,
} from "antd";
import { BellOutlined } from "@ant-design/icons";
import { NavLink } from "react-router-dom";
import { useDispatch } from "react-redux";
import { clearToken } from "../../../store/authSlice";

import Logo from "@assets/img/Logo";
import VnLogo from "@assets/img/vietnam.png";

import * as S from "./Header";

export default function Header() {
  const { Header: Hd } = Layout;
  const [current, setCurrent] = useState("2");
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(clearToken());
    message.success("Đăng xuất thành công!");
  };

  const items = [
    {
      key: "2",
      label: (
        <NavLink style={{ fontWeight: 500 }} end>
          Workspaces
        </NavLink>
      ),
    },
    {
      key: "3",
      label: <NavLink style={{ fontWeight: 500 }}>Lời mời</NavLink>,
    },
  ];

  const accountItems = [
    {
      key: "1",
      label: (
        <S.StyledMenuItem style={{ cursor: "auto" }}>
          <Avatar style={{ backgroundColor: "#f56a00" }}></Avatar>
          <div
            style={{
              marginLeft: 10,
              display: "flex",
              flexDirection: "column",
            }}
          >
            <Typography.Text style={{ margin: 0 }}></Typography.Text>
            <Typography.Text
              type="secondary"
              style={{ margin: 0 }}
            ></Typography.Text>
          </div>
        </S.StyledMenuItem>
      ),
    },
    { type: "divider" },
    {
      key: "2",
      label: (
        <NavLink to={""}>
          <S.StyledMenuItem style={{ width: 170 }}>
            <Typography.Text style={{ margin: 0 }}>{"Profile"}</Typography.Text>
          </S.StyledMenuItem>
        </NavLink>
      ),
    },
    {
      key: "3",
      label: (
        <NavLink to={""}>
          <S.StyledMenuItem>
            <Typography.Text style={{ margin: 0 }}>
              Đổi mật khẩu
            </Typography.Text>
          </S.StyledMenuItem>
        </NavLink>
      ),
    },
    { type: "divider" },
    {
      key: "5",
      label: (
        <S.StyledMenuItem onClick={handleLogout}>
          <Typography.Text style={{ margin: 0 }} type="danger">
            Đăng xuất
          </Typography.Text>
        </S.StyledMenuItem>
      ),
    },
  ];

  const onClick = (e) => {
    console.log("click ", e);
    setCurrent(e.key);
  };

  return (
    <S.HeaderWrapper>
      <Hd>
        <Logo style={{ width: "8%" }} />
        <S.MenuWrapper style={{ width: "76%", paddingLeft: 20 }}>
          <Menu
            items={items}
            mode="horizontal"
            style={{ borderBottom: "none" }}
            onClick={onClick}
            selectedKeys={[current]}
          />
        </S.MenuWrapper>
        <Row style={{ width: "30%" }} justify="end" align="middle">
          <Badge style={{ marginRight: 35, marginTop: 5 }}>
            <BellOutlined
              style={{ marginRight: 35, fontSize: 16, marginTop: 8 }}
            />
          </Badge>

          <Dropdown
            overlayStyle={{ minWidth: 150 }}
            menu={{ items: accountItems, selectedKeys: [] }}
            placement="bottomRight"
          >
            <Space align="center" size={8} style={{ cursor: "pointer" }}>
              <Avatar style={{ backgroundColor: "#f56a00" }}></Avatar>
            </Space>
          </Dropdown>

          <Button
            size="small"
            type="text"
            style={{ marginLeft: 15, marginTop: 5 }}
          >
            <img alt="flag" src={VnLogo} style={{ width: 20, height: 20 }} />
          </Button>
        </Row>
      </Hd>
    </S.HeaderWrapper>
  );
}
