import { Layout } from 'antd';

import Header from '../Header';
import * as S from './styled';

export default function UserLayout({ children }) {
  const { Content } = Layout;

  return (
    <S.UserLayoutWrap>
      <Layout style={{ minHeight: '100vh' }}>
        <Header />
        <Content>
          <S.ContainerWrapper>
            {children}
          </S.ContainerWrapper>
        </Content>
      </Layout>
    </S.UserLayoutWrap>
  );
}
