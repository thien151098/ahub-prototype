import styled from "styled-components";
import { Layout } from "antd";

export const UserLayoutWrap = styled(Layout)`
  overflow: hidden;
  .ant-layout-content {
    padding: 32px 32px 0px;
    background: rgb(240, 242, 245);
    height: calc(-65px + 100vh);
  }
`;

export const ContainerWrapper = styled.div`
  height: calc(100vh - 105px);
  max-width: 84%;
  margin: 0 auto;
`;
