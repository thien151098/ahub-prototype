import { useState, useEffect } from "react";
import { Upload, message } from "antd";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";

import * as S from "./styled";

const getBase64 = (img, callback) => {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
};

export default function ImageUploader({ label, onChange, value, isLoading }) {
  const [imageUrl, setImageUrl] = useState(() =>
    typeof value === "string" ? value : undefined
  );

  useEffect(() => {
    if (typeof value === "string") {
      setImageUrl(value);
    }
  }, [value]);

  const beforeUpload = (file) => {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
      return false;
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("Image must smaller than 2MB!");
      return false;
    }

    getBase64(file, (base64) => {
      onChange?.(file);
      setImageUrl(base64);
    });

    return false;
  };

  const uploadButton = (
    <div>
      {isLoading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>{label}</div>
    </div>
  );

  return (
    <S.StyledUpload
      listType="picture-card"
      showUploadList={false}
      beforeUpload={beforeUpload}
      style={{ height: "100%", width: "100%" }}
    >
      {imageUrl ? (
        <img
          src={imageUrl}
          alt={label}
          style={{ width: "100%", height: "100%", objectFit: "contain" }}
        />
      ) : (
        uploadButton
      )}
    </S.StyledUpload>
  );
}
