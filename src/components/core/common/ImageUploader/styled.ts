import { Upload } from "antd";
import styled from "styled-components";

export const StyledUpload = styled(Upload)`
  .ant-upload {
    width: 152px !important;
    height: 152px !important;
    overflow: hidden;
  }
`;
