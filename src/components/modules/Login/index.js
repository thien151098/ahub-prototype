import {
  Form,
  Input,
  Button,
  Checkbox,
  Space,
  Typography,
  Row,
  Col,
  message,
} from "antd";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { MailOutlined } from "@ant-design/icons";
import { useDispatch } from "react-redux";
import { setToken } from "../../../store/authSlice";

import LoginImg from "@assets/img/LoginImg";
import Logo from "@assets/img/Logo";
import loginImage from "@assets/img/loginImage.png";

import { Link } from "react-router-dom";

import * as S from "./styled";

export default function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onSubmit = async (data) => {
    try {
      const response = await axios.post(
        "https://api-dev.ahub.vn/v1/n/auth/login/",
        {
          email: data?.email,
          password: data?.password,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const token = response?.data?.access_token;
      dispatch(setToken(token));
      localStorage.setItem("token", token);
      message.success("Đăng nhập thành công!");
      navigate("/workspace");
    } catch (error) {
      message.error("Đăng nhập thất bại, vui lòng thử lại.");
      console.error("Error »»»»»", error);
    }
  };

  return (
    <S.LoginWrapper>
      <S.FormWrapper>
        <Row>
          <Col xl={11} sm={0} xs={0}>
            <S.LeftFormWrapper>
              <S.LeftHeader>
                <div className="logo">
                  <Logo />
                </div>
                <Link to="/register">
                  <Button size="large" type="primary">
                    Tạo tài khoản
                  </Button>
                </Link>
              </S.LeftHeader>
              <div className="left-image">
                <img
                  src={loginImage}
                  alt="login"
                  className="near-xxl-not-show"
                />
                <LoginImg className="near-xxl-show" />
              </div>

              <S.LeftFooter>
                <p className="left-footer__slogan--1">
                  Quản lý trung tâm dễ dàng và hiệu quả
                </p>
                <p className="left-footer__slogan--2">GIẢI PHÁP TOÀN DIỆN</p>
              </S.LeftFooter>
            </S.LeftFormWrapper>
          </Col>

          <Col xl={13} sm={24} xs={24}>
            <S.RightFormWrapper>
              <S.PositionLogo>
                <div className="logo">
                  <Logo />
                </div>
                <Link to="/register">
                  <Button size="large" type="primary">
                    Tạo Tài Khoản
                  </Button>
                </Link>
              </S.PositionLogo>
              <div className="form-header">
                <p className="md-show">Đăng nhập vào tài khoản</p>
              </div>
              <div className="form-content over-ride-input over-ride-checkbox ">
                <Form
                  onFinish={onSubmit}
                  autoComplete="off"
                  scrollToFirstError
                  layout="vertical"
                >
                  <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                      {
                        type: "email",
                        message: "Email không hợp lệ",
                      },
                      {
                        required: true,
                        message: "Vui lòng nhập email",
                      },
                    ]}
                  >
                    <Input
                      suffix={<MailOutlined className="site-form-item-icon" />}
                    />
                  </Form.Item>

                  <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập mật khẩu",
                      },
                    ]}
                  >
                    <Input.Password />
                  </Form.Item>

                  <Form.Item
                    name="remember"
                    valuePropName="checked"
                    style={{ marginBottom: 40 }}
                  >
                    <Space
                      style={{ justifyContent: "space-between", width: "100%" }}
                    >
                      <Checkbox className="check-box-group__remember">
                        Nhớ thông tin cho lần sau
                      </Checkbox>
                      <Typography.Link>Quên mật khẩu?</Typography.Link>
                    </Space>
                  </Form.Item>

                  <Form.Item style={{ marginTop: 80 }}>
                    <Button
                      size="large"
                      htmlType="submit"
                      type="primary"
                      style={{ width: "100%" }}
                    >
                      Đăng Nhập
                    </Button>
                  </Form.Item>
                </Form>
              </div>
            </S.RightFormWrapper>
          </Col>
        </Row>
      </S.FormWrapper>
    </S.LoginWrapper>
  );
}
