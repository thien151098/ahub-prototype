import { Input, Form, Row, Button, Typography, message } from "antd";
import axios from "axios";
import { MailOutlined, PhoneOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

import Logo from "@assets/img/Logo";

import * as S from "./styled";

export default function Register() {
  const navigate = useNavigate();

  const handleRegister = async (data) => {
    try {
      const response = await axios.post(
        "https://api-dev.ahub.vn/v1/n/auth/register/",
        {
          email: data.email,
          password: data.password,
          confirm_password: data.confirm,
          phone_number: data.phone,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      message.success("Đăng ký thành công!");
      console.log("Response »»»»»", response);
      navigate("/login");
    } catch (error) {
      message.error("Đăng ký thất bại, vui lòng thử lại.");
      console.error("Error »»»»»", error);
    }
  };

  return (
    <S.RegisterWrapper>
      <S.SignUpFormWrapper>
        <Form
          onFinish={handleRegister}
          autoComplete="off"
          scrollToFirstError
          layout="vertical"
        >
          <Row
            style={{
              width: "100%",
              justifyContent: "space-between",
              paddingBottom: 40,
            }}
          >
            <div>
              <Logo />
            </div>
            <div>
              <Typography.Title className="register-title" level={4}>
                TẠO TÀI KHOẢN MỚI
              </Typography.Title>
              <Typography.Text className="register-sub-title">
                Nhanh chóng và dễ dàng
              </Typography.Text>
            </div>
          </Row>

          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                type: "email",
                message: "Email không hợp lệ",
              },
              {
                required: true,
                message: "Vui lòng nhập email",
              },
            ]}
          >
            <Input prefix={<MailOutlined className="site-form-item-icon" />} />
          </Form.Item>

          <Form.Item
            label="Mật khẩu"
            name="password"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mật khẩu",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="confirm"
            label="Nhập lại Mật khẩu"
            dependencies={["password"]}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập lại mật khẩu",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error("Mật khẩu xác nhận không khớp")
                  );
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="phone"
            label="Phone Number"
            rules={[
              { required: true, message: "Vui lòng nhập số điện thoại" },
              {
                pattern: /^[0-9]{10,11}$/,
                message:
                  "Số điện thoại không hợp lệ. Vui lòng nhập 10-11 chữ số.",
              },
            ]}
          >
            <Input prefix={<PhoneOutlined />} style={{ width: "100%" }} />
          </Form.Item>

          <Row justify="center" style={{ marginTop: 60 }}>
            <Button
              style={{ width: "100%" }}
              size="large"
              type="primary"
              htmlType="submit"
            >
              Đăng Ký
            </Button>
          </Row>
        </Form>

        <Row style={{ marginTop: 20 }} justify="center">
          <span style={{ marginRight: 10, fontWeight: 600 }}>
            Bạn đã có tài khoản
          </span>{" "}
          <Link style={{ fontWeight: "bold", color: "#c1112f" }} to={"/login"}>
            Đăng nhập ở đây
          </Link>
        </Row>
      </S.SignUpFormWrapper>
    </S.RegisterWrapper>
  );
}
