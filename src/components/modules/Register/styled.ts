import styled from "styled-components";
import color from "../../../styles/themes/default/colors";

export const RegisterWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background-color: ${color?.backgroundColorLogin};

  .ant-btn-primary {
    background: ${color?.buttonRed};

    &:hover {
      background: #e65043 !important;
    }
  }
`;

export const SignUpFormWrapper = styled.div`
  width: 645px;
  border-radius: 50px;
  background-color: #fff;
  padding: 50px;
  min-height: 700px;

  .register-title {
    font-size: 20px;
    font-family: mon-semi-bold;
    text-transform: uppercase;
    margin: 0 !important;
  }

  .register-sub-title {
    font-size: 20px !important;
    line-height: 30px;
    font-family: mon-regular;
    margin: 0 !important;
  }
`;
