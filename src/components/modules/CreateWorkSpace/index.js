import { useState } from "react";
import {
  Form,
  Input,
  Row,
  Col,
  Button,
  Typography,
  Select,
  message,
} from "antd";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import axios from "axios";

import ImageUploader from "@components/core/common/ImageUploader";

import * as S from "./styled";

export default function CreateWorkSpace() {
  const [form] = Form.useForm();
  const { Option } = Select;
  const navigate = useNavigate();

  const [logoFile, setLogoFile] = useState(null);

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select style={{ width: 70 }}>
        <Option value="84">+84</Option>
        <Option value="85">+85</Option>
      </Select>
    </Form.Item>
  );

  const onFinish = async (values) => {
    const formData = new FormData();
    formData.append("name", values.name);
    formData.append("address", values.address);
    formData.append("email", values.email);
    formData.append("phone_number", values.phone);
    formData.append("workspace", values.workspace);
    formData.append("website_url", values.url);
    if (logoFile) {
      formData.append("logo", logoFile);
    }

    try {
      const response = await axios.post(
        "https://api-dev.ahub.vn/v1/n/workspace/",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      message.success("Tạo mới Workspace thành công!");
      navigate("/workspace");
    } catch (error) {
      message.error("Tạo mới Workspace thất bại, vui lòng thử lại.");
      console.error("Error »»»»»", error);
    }
  };

  const handleLogoChange = (file) => {
    setLogoFile(file);
  };

  return (
    <S.CreateWorkSpaceWrapper>
      <S.TitleRow>
        <Typography.Title style={{ marginBottom: 0, fontSize: "20px" }}>
          Tạo Mới Workspace
        </Typography.Title>
      </S.TitleRow>

      <S.ContentWrapper>
        <Form form={form} layout="vertical" onFinish={onFinish}>
          <Row justify="center" align="middle" style={{ marginTop: 20 }}>
            <Form.Item name="logo" style={{ marginBottom: 0 }}>
              <ImageUploader label={"logo"} onChange={handleLogoChange} />
            </Form.Item>
          </Row>

          <Row gutter={64} style={{ marginTop: 40 }}>
            <Col span={12}>
              <Form.Item
                label="Tên"
                name="name"
                rules={[
                  { required: true, message: "Vui lòng nhập tên Workspace" },
                ]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Địa chỉ" name="address">
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Email" name="email">
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="phone"
                label="Số điện thoại"
                rules={[
                  {
                    pattern: /^[0-9]{10,11}$/,
                    message:
                      "Số điện thoại không hợp lệ. Vui lòng nhập 10-11 chữ số.",
                  },
                ]}
              >
                <Input addonBefore={prefixSelector} style={{ width: "100%" }} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Workspace"
                name="workspace"
                rules={[
                  { required: true, message: "Vui lòng nhập trường Workspace" },
                ]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Đường dẫn website" name="url">
                <Input />
              </Form.Item>
            </Col>
          </Row>

          <Row justify="center" style={{ marginTop: 70 }}>
            <Button
              style={{ marginRight: 10 }}
              onClick={() => navigate("/workspace")}
            >
              Thoát
            </Button>
            <Button style={{ marginLeft: 10 }} htmlType="submit" type="primary">
              Tạo mới
            </Button>
          </Row>
        </Form>
      </S.ContentWrapper>
    </S.CreateWorkSpaceWrapper>
  );
}
