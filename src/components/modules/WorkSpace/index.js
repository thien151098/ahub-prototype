import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Button, Space, Typography, message } from "antd";
import { Link } from "react-router-dom";
import axios from "axios";

import WorkspaceCard from "@components/core/common/WorkspaceCard";

import * as S from "./styled";

export default function WorkSpace() {
  const [workspaces, setWorkspaces] = useState([]);
  const [loading, setLoading] = useState(true);

  const token = useSelector((state) => state.auth.token);

  useEffect(() => {
    const fetchWorkspaces = async () => {
      try {
        const response = await axios.get(
          "https://api-dev.ahub.vn/v1/n/workspace/",
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
            },
          }
        );
        setWorkspaces(response?.data?.results);
        setLoading(false);
      } catch (error) {
        message.error("Failed to fetch workspaces");
        setLoading(false);
      }
    };

    if (token) {
      fetchWorkspaces();
    }
  }, [token]);

  return (
    <S.WorkSpaceWrapper>
      <S.TitleRow>
        <Typography.Title style={{ marginBottom: 0, fontSize: "20px" }}>
          Danh sách Workspace
        </Typography.Title>
        <div>
          <Link to={"/workspace/create"}>
            <Button type="primary">Tạo mới Workspace</Button>
          </Link>
        </div>
      </S.TitleRow>
      <div style={{ minHeight: 257 }}>
        <Space size={32} wrap>
          {loading ? (
            <Typography.Text>Loading...</Typography.Text>
          ) : (
            workspaces?.map((workspace) => (
              <WorkspaceCard
                key={workspace?.id || ""}
                name={workspace?.name || ""}
                email={workspace?.email || ""}
                logo={workspace?.logo || ""}
              />
            ))
          )}
        </Space>
      </div>
    </S.WorkSpaceWrapper>
  );
}
