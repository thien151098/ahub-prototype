import colors from "@styles/themes/default/colors";
import { Row } from "antd";
import styled from "styled-components";

export const WorkSpaceWrapper = styled.div`
  max-width: 1600px;
  margin: 0 auto;
  height: calc(100vh - 64px);
  padding: 0 30px;

  .ant-btn-primary {
    background: ${colors?.buttonRed};

    &:hover {
      background: #e65043 !important;
    }
  }
`;

export const TitleRow = styled(Row)`
  margin-bottom: 26px;
  min-height: 32px;
  justify-content: space-between;
  align-items: center;
`;
