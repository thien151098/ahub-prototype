import { css } from "styled-components";

const utility = css`
  .container {
    width: 100%;
    max-width: 1270px;
    margin: 0 auto;
    padding-left: 15px;
    padding-right: 15px;

    @media ${(props) => props.theme.breakpoints.xxlMin} {
      max-width: 1420px;
    }

    @media ${(props) => props.theme.breakpoints.xxlMax} {
      max-width: 1270px;
    }

    @media ${(props) => props.theme.breakpoints.xlMax} {
      max-width: 960px;
    }

    @media ${(props) => props.theme.breakpoints.lgMax} {
      max-width: 740px;
    }

    @media ${(props) => props.theme.breakpoints.mdMax} {
      max-width: 100%;
    }
  }

  .container-fluid {
    max-width: 100%;
    margin: 0 auto;
    padding-left: 15px;
    padding-right: 15px;
  }

  /* custom */
  .text-truncate-1 {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
  }

  .text-truncate-2 {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }

  .text-truncate-3 {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
  }

  .text-truncate-4 {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
  }

  .text-truncate-5 {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 5;
    -webkit-box-orient: vertical;
  }

  /* ANTD Dropdown */
  .ant-dropdown-menu-title-content {
    .dropdown-item {
      display: flex;
      gap: 8px;
      label {
        cursor: pointer;
      }
    }
  }

  /* ANTD Drawer */

  /* ANTD Switch */

  .ant-switch.ant-switch-checked {
    background-color: ${(props) => props.theme.colors.primaryColor};
  }

  .ant-switch.ant-switch-checked:hover:not(.ant-switch-disabled) {
    opacity: 0.8;
    background-color: ${(props) => props.theme.colors.primaryColor};
  }

  /* ANTD Breadcrumb */

  .active-page {
    font-weight: ${(props) => props.theme.fontWeight.semiBold};
    font-size: ${(props) => props.theme.fontSize.md};
    line-height: ${(props) => props.theme.fontSize.xxl};

    color: ${({ theme }) => theme.colors.primaryColor};
    cursor: pointer;
  }

  .page-bread {
    font-weight: ${(props) => props.theme.fontWeight.semiBold};
    font-size: ${(props) => props.theme.fontSize.md};
    line-height: ${(props) => props.theme.fontSize.xxl};

    color: #777777;
    cursor: pointer;
  }

  .ant-breadcrumb {
    margin-bottom: 60px;
  }

  /* ANTD checkbox */
  .ant-checkbox-group {
    display: grid !important;
  }
  .ant-checkbox-group-item + .ant-checkbox-group-item {
    margin-left: 0;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${({ theme }) => theme.colors.primaryColor} !important;
    border-color: ${({ theme }) => theme.colors.primaryColor} !important;
  }
  .ant-checkbox-wrapper:hover .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${({ theme }) => theme.colors.primaryColor} !important;
  }
  .ant-checkbox-checked:after {
    border: none !important;
  }
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner {
    border-color: ${({ theme }) => theme.colors.primaryColor} !important;
  }

  // ANTD Slider

  .ant-slider-track {
    background-color: ${({ theme }) => theme.colors.primaryColor} !important;
  }

  .ant-slider .ant-slider-handle::after {
    box-shadow: 0 0 0 2px ${({ theme }) => theme.colors.primaryColor} !important;
  }

  // ANTD DRAWER

  .ant-drawer-header {
    border: none !important;
  }

  .ant-drawer-close {
    position: absolute;
    right: 10px;
    top: 10px;
  }

  .anticon-close {
    svg {
      width: 16px;
      height: 16px;
    }
  }


  // ANTD RADIO

  .ant-radio-wrapper .ant-radio-checked .ant-radio-inner {
    border-color: ${(props) => props?.theme?.colors?.primaryColor} !important;
    background-color: ${(props) => props?.theme?.colors?.white} !important;

    ::after {
      transform: scale(0.5);
      background-color: ${(props) =>
        props?.theme?.colors?.primaryColor} !important;
    }
  }

  .ant-radio-wrapper:hover .ant-radio-inner {
    border-color: ${({ theme }) => theme.colors.primaryColor} !important;
  }

  // Hide Arrows From Input Number

  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  // ANTD SELECT

  .ant-select-single.ant-select-show-arrow .ant-select-selection-placeholder {
    display: flex;
    align-items: center;
  }

  .ant-select-item-option-content {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    white-space: break-spaces !important;
    word-break: break-word !important;

    gap: 8px;

    .flag {
      position: relative;

      width: 30px;
      height: 20px;

      img {
        object-fit: contain;
      }
    }
  }

  .menu-header {
    .ant-select-item-option {
      padding-top: 8px;
      font-weight: 500;
      font-size: 16px;
      line-height: 26px;
      border-radius: 0;
      border-bottom: 1px solid #c0c0c0;

      &:last-child {
        margin-bottom: 8px;
      }
    }
  }

  // REACT PHONE NUMBER
  .react-tel-input {
    .form-control {
      width: 100%;
      height: 48px;
      border-radius: 10px;

      &:hover,
      &:active,
      &:focus {
        border-color: ${({ theme }) => theme.colors.primaryColor} !important;
      }
    }

    .flag-dropdown {
      width: 60px;
      border: none !important;
      background-color: transparent !important;

      .flag {
        transform: scale(1.2);
      }

      .selected-flag {
        width: 100%;
        padding-left: 15px;
        border-radius: 40px;
        background-color: transparent !important;
      }

      .search {
        padding-left: 0px !important;
        z-index: 9;
      }
    }

    .selected-flag:hover,
    .selected-flag:active {
      background: transparent !important;
    }
  }

  // CUSTOMIZE ACCOUNT USER MOBILE DRAWER

  .ant-drawer-mask {
    background: transparent !important;
  }

  .ant-drawer-content-wrapper {
    margin-top: 150px;
    border-radius: 20px;
    background: #262631 !important;

    height: auto !important;
  }

  .account-mobile {
    position: relative;
    height: calc(100vh - ${({ theme }) => theme.height.header}) !important;

    *::-webkit-scrollbar {
      width: 0;
    }

    &.ant-drawer-content {
      overflow: unset;
      margin-top: ${({ theme }) => theme.height.header};

      &::-webkit-scrollbar {
        width: 0;
        height: 0;
      }
    }

    .ant-drawer-header-title {
      display: none;
    }

    .ant-drawer-body {
      padding: 0;

      box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
      border-radius: 10px;
    }

    .back-icon {
      position: absolute;
      right: 0;
      top: 60px;

      cursor: pointer;
      transform: translateX(50%);
      z-index: 9;
    }

    .ant-drawer-header {
      display: none;
    }
  }

  // ANTD TABLE
  .ant-table-column-sorters {
    align-items: baseline;
  }

  .ant-table-wrapper .ant-table-thead th.ant-table-column-sort {
    background: ${(props) => props.theme.colors.primaryColor};
  }

  .ant-table-wrapper .ant-table-thead th.ant-table-column-has-sorters:hover {
    background: ${(props) => props.theme.colors.primaryColor};
  }

  .ant-table-wrapper .ant-table-column-sorter {
    color: ${(props) => props.theme.colors.white};
  }

  // ANTD MODAL

  .ant-modal {
    margin: 10px 0;
  }

  .custom-svg {
    width: 20px;
    height: 22px;

    div {
      width: 20px;
      height: 20px;
    }
  }

  .text-center {
    text-align: center;
  }

  .justify-center {
    justify-content: center;
  }

  .vertical-center {
    vertical-align: middle;
  }

  .ant-message {
    .ant-message-custom-content {
      display: flex;

      > span.anticon {
        width: 20px;
      }
    }
  }

  .table-column-checkbox-header {
    display: flex;
    align-items: flex-end;
    gap: 8px;

    .ant-checkbox-wrapper .ant-checkbox .ant-checkbox-inner {
      border-color: white !important;
      margin-top: -2px;
    }

    .ant-checkbox-wrapper:hover .ant-checkbox .ant-checkbox-inner,
    .ant-checkbox-wrapper:hover
      .ant-checkbox-checked:not(.ant-checkbox-disabled):after {
      border-color: white !important;
    }
  }

  .table-column-title-header {
    display: flex;
    align-items: center;
    gap: 10px;

    p {
      white-space: nowrap;
    }
  }

  .table-column-checkbox-cell {
    height: 100%;
    display: flex;
    align-items: flex-end;
    gap: 8px;

    .ant-checkbox-wrapper .ant-checkbox .ant-checkbox-inner {
      margin-top: -2px;
    }
  }

  .table-action-wrapper {
    display: flex;
    align-items: center;
    gap: 8px;

    svg {
      line-height: 1;
      cursor: pointer;
    }
  }

  .table-text-dark {
    font-weight: ${(props) => props.theme.fontWeight.regular};
    font-size: ${(props) => props.theme.fontSize.md};
    line-height: ${(props) => props.theme.fontSize.xxl};
    color: #171717;
  }

  .table-text-clickable {
    cursor: pointer;
  }

  .table-text-name {
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 2; /* number of lines to show */
    line-clamp: 2;
    -webkit-box-orient: vertical;
  }

  .add-question-modal {
    width: 1000px !important;

    & > p {
      font-weight: ${(props) => props.theme.fontWeight.bold};
      font-size: ${(props) => props.theme.fontSize.xxl};
      line-height: 32px;
      color: ${(props) => props.theme.colors.primaryColor} !important;
    }

    .ant-modal-content {
      border-radius: ${(props) => props.theme.radius.mediumRadius} !important;

      .ant-modal-close-x {
        color: #171717;
      }

      .add-question-modal-body {
        font-weight: ${(props) => props.theme.fontWeight.bold};
        font-size: ${(props) => props.theme.fontSize.base};
        line-height: ${(props) => props.theme.fontSize.xl};
        color: #171717;

        text-align: center;
      }
    }

    @media ${(props) => props.theme.breakpoints.xlMax} {
      width: 100% !important;
    }
  }

  .delete-question-modal {
    width: 608px !important;

    & > p {
      font-weight: ${(props) => props.theme.fontWeight.bold};
      font-size: ${(props) => props.theme.fontSize.xxl};
      line-height: 32px;
      color: ${(props) => props.theme.colors.primaryColor} !important;

      text-transform: uppercase;
    }

    .ant-modal-content {
      border-radius: ${(props) => props.theme.radius.mediumRadius} !important;

      .ant-modal-close-x {
        color: #171717;
      }

      .delete-question-modal-body {
        font-weight: ${(props) => props.theme.fontWeight.bold};
        font-size: ${(props) => props.theme.fontSize.base};
        line-height: ${(props) => props.theme.fontSize.xl};
        color: #171717;

        text-align: center;
      }
    }

    @media ${(props) => props.theme.breakpoints.smMax} {
      width: 100% !important;
    }
  }

  .detail-question-modal {
    & > p {
      color: ${(props) => props.theme.colors.black} !important;
      width: 100%;
      text-align: left;
      padding-left: 16px;
    }

    .ant-modal-content {
      border-radius: ${(props) => props.theme.radius.mediumRadius} !important;

      .ant-modal-close-x {
        color: black;
      }

      .detail-question-modal-body {
        padding-top: 0px;
        padding-bottom: 24px;
      }
    }
  }

  .filter-question-modal {
    width: 608px !important;

    & > p {
      font-weight: ${(props) => props.theme.fontWeight.bold};
      font-size: ${(props) => props.theme.fontSize.xxl};
      line-height: 32px;
      color: ${(props) => props.theme.colors.primaryColor} !important;

      text-transform: uppercase;
    }

    .ant-modal-content {
      border-radius: ${(props) => props.theme.radius.mediumRadius} !important;

      .ant-modal-close-x {
        color: #171717;
      }

      .filter-question-modal-body {
        padding-bottom: 24px;

        p {
          font-weight: ${(props) => props.theme.fontWeight.semiBold};
          font-size: ${(props) => props.theme.fontSize.base};
          line-height: ${(props) => props.theme.fontSize.xl};
          color: #171717;

          text-transform: unset;
        }

        .ant-form-item {
          margin-bottom: 16px;
        }
      }
    }

    .radio-box-center {
      display: flex;
      justify-content: space-between;
    }

    @media ${(props) => props.theme.breakpoints.smMax} {
      width: 100% !important;
    }
  }

  // CKEditor

  .ck-editor__editable {
    min-height: 150px;
  }

  .ck-rounded-corners .ck.ck-editor__top .ck-sticky-panel .ck-toolbar,
  .ck.ck-editor__top .ck-sticky-panel .ck-toolbar.ck-rounded-corners {
    border-top-left-radius: 10px !important;
    border-top-right-radius: 10px !important;
  }

  .ck-rounded-corners .ck.ck-editor__main > .ck-editor__editable,
  .ck.ck-editor__main > .ck-editor__editable.ck-rounded-corners {
    border-bottom-left-radius: 10px !important;
    border-bottom-right-radius: 10px !important;
  }

  .ck.ck-dropdown.ck-heading-dropdown .ck-dropdown__button .ck-button__label {
    color: ${(props) => props.theme.colors.primaryColor} !important;
  }

  [dir="ltr"] .ck.ck-dropdown .ck-dropdown__arrow {
    color: ${(props) => props.theme.colors.primaryColor} !important;
  }

  .ck.ck-icon.ck-icon_inherit-color,
  .ck.ck-icon.ck-icon_inherit-color * {
    color: ${(props) => props.theme.colors.primaryColor} !important;
  }

  .button-secondary {
    color: ${(props) => props.theme.colors.white} !important;
    background-color: ${(props) => props.theme.colors.primaryColor} !important;

    &:hover {
      background-color: ${(props) => props.theme.colors.white} !important;
      color: ${(props) => props.theme.colors.primaryColor} !important;
    }

    &.disabled {
      opacity: 0.5;

      &:hover {
        color: ${(props) => props.theme.colors.white} !important;
        background-color: ${(props) =>
          props.theme.colors.primaryColor} !important;
        opacity: 0.5;
      }
    }
  }

  @media ${(props) => props.theme.breakpoints.lgMax} {
    .ant-breadcrumb {
      margin-bottom: 50px;
    }
  }

  @media ${(props) => props.theme.breakpoints.mdMax} {
    .ant-breadcrumb {
      margin-bottom: 42px;
    }
  }

  @media ${(props) => props.theme.breakpoints.smMax} {
    .ant-breadcrumb {
      margin-bottom: 35px;
    }
  }

  .text-capitalize {
    text-transform: capitalize;
  }

  .text-transform-none {
    text-transform: none;
  }

  .ant-modal-body {
    .modal-registertration-success {
      padding-top: 0px;
    }

    div {
      &.content-modal-success {
        padding-top: 0px;
      }
    }
  }

  .ant-float-btn-body {
    background-color: #543e93 !important;
  }
  .ant-float-btn-icon {
    color: #ffff !important;
  }

  .rbc-overlay {
    max-height: 50vh;
    width: 300px;
    overflow-y: auto;
    padding: 0;

    &::-webkit-scrollbar {
      width: 4px;
      height: 4px;
    }

    .rbc-overlay-header {
      position: sticky;
      top: 0;
      width: 100%;
      margin: 0;
      z-index: 1;
      background: #fff;
    }

    .rbc-event {
      background: none;
      border-bottom: 1px solid #ddd;
      padding: 10px;
    }
  }
`;

export default utility;
