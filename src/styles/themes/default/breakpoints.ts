const sizes = {
  xxl: "1600.1px",
  xl: "1200.1px",
  lg: "991.1px",
  md: "768.1px",
  sm: "576.1px",
  xs: "375.1px",
};

const breakpoints = {
  /* Min-width */
  xxlMin: `(min-width: ${sizes.xxl})`,
  xlMin: `(min-width: ${sizes.xl})`,
  lgMin: `(min-width: ${sizes.lg})`,
  mdMin: `(min-width: ${sizes.md})`,
  smMin: `(min-width: ${sizes.sm})`,
  xsMin: `(min-width: ${sizes.xs})`,

  /* Max-width */
  xxlMax: `(max-width: ${sizes.xxl})`,
  xlMax: `(max-width: ${sizes.xl})`,
  lgMax: `(max-width: ${sizes.lg})`,
  mdMax: `(max-width: ${sizes.md})`,
  smMax: `(max-width: ${sizes.sm})`,
  xsMax: `(max-width: ${sizes.xs})`,
};

export default breakpoints;
