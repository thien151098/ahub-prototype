const themeColors = {
  // Primary
  purple1: "#543E93",
  purple2: "#600B76",
  purple3: "#730D8C",
  purple4: "#9439AB",
  purple5: "#7D14FD",
  purple6: "#9C90BD",

  // Secondary
  blue1: "#3424FF",
  blue2: "#3821DF",
  blue3: "#5547FF",
  blue4: "#695EFF",
  gray1: "#434243",

  // Tertiary
  yelow1: "#FFD006",
  yelow2: "#FFD72E",
  yelow3: "#FFDF58",
  yelow4: "#FFE57C",
  yellow5: "#F8B700",
  yellow6: "#E9912A",
  yellow7: "#FFF1E1",
  yellow8: "#c6963a",

  // Text
  text1: "#19171A",
  text2: "#7B7583",
  text3: "#434343",
  text4: "#747474",
  text5: "#d12f25",

  // Background
  background1: "#EDEDED",
  background2: "#F8F7FA",
  background3: "#FFFFFF",
  background4: "#BDF7C5",
  background5: "#FBDA7D",
  background6: "rgb(193, 17, 47)",
  background7: "rgb(250, 250, 251)",

  // Notification
  green1: "#07D95A",
  green2: "#9CF0BD",
  red1: "#F8183E",
  red2: "#FCA3B2",

  // Gradient
  gradient1: "#F05F5F",
  gradient2: "#B33030",
  gradient3: "#7F26CD",
  gradient4: "#6A15CE",
  gradient5: "#D00000",

  // Button
  gray: "#B9B5C2",
  gray2: "#8D8C8E",
  gray3: "#666666",
  gray4: "#A2A3A5",
  red: "#da271c",

  // Default Colors
  white: "#FFFFFF",
  black: "#000000",
  green: "#47C751",
  orange: "#FF7200",
  tetriaryOrange: "#FFB67B",
  darkGray: "#171717",
  silver: "#C0C0C0",
  darkGreen: "#279415",
  danger: "#EF4949",

  // Border
  border1: "#F3F3F3",
  border2: "#E6D3FF",
};

const colors = {
  primaryColor: themeColors.red,
  secondaryColor: themeColors.blue2,
  tertiaryColor: themeColors.yelow2,
  textColor: themeColors.text2,
  textWhite: themeColors.white,
  textBoldColor: themeColors.text1,
  backgroundColor: themeColors.background1,
  shadowColor: themeColors.gradient1,
  notificationSuccess: themeColors.green1,
  notificationError: themeColors.red1,
  buttonGradient: `linear-gradient(180deg, ${themeColors.gradient1} 0%, ${themeColors.gradient2} 100%)`,
  buttonActive: themeColors.gradient1,
  buttonDisable: themeColors.gray,
  orange: themeColors.orange,
  white: themeColors.white,
  black: themeColors.black,
  border: themeColors.border1,
  textGray: themeColors.text3,
  successColor: themeColors.green,
  textSee: themeColors.gray4,
  textSmall: themeColors.darkGray,
  backgroundColorLogin: themeColors.background6,
  buttonRed: themeColors.red,
  backgroundGrayLogin: themeColors.background7,

  //Other color
  other: themeColors,
};

export default colors;
