import { Suspense, lazy } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
  useLocation,
} from "react-router-dom";
import { ThemeProvider } from "styled-components";
import { useSelector } from "react-redux";

import WorkSpace from "@components/modules/WorkSpace";
import CreateWorkSpace from "@components/modules/CreateWorkSpace";

import { theme } from "./styles/themes";
import { NONE_LAYOUT } from "./settings/constants";
import GlobalStyle from "./styles/global";

const Login = lazy(() => import("./components/modules/Login"));
const Register = lazy(() => import("./components/modules/Register"));
const UserLayoutWrap = lazy(() => import("./components/layout/UserLayout"));

const Home = () => <div>Home Page</div>;

const RenderLayout = ({ Component, pageProps }) => {
  const location = useLocation();
  const token = useSelector((state) => state?.auth?.token);

  if (!token && !NONE_LAYOUT.includes(location?.pathname)) {
    return <Navigate to="/login" />;
  }

  if (NONE_LAYOUT.includes(location?.pathname)) {
    return <Component {...pageProps} />;
  }

  return (
    <UserLayoutWrap>
      <Component {...pageProps} />
    </UserLayoutWrap>
  );
};

function App() {
  return (
    <ThemeProvider theme={theme.default}>
      <Router>
        <Suspense fallback={<div>Loading...</div>}>
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/" element={<RenderLayout Component={Home} />} />
            <Route
              path="/workspace"
              element={<RenderLayout Component={WorkSpace} />}
            />
            <Route
              path="/workspace/create"
              element={<RenderLayout Component={CreateWorkSpace} />}
            />
          </Routes>
        </Suspense>
      </Router>
      <GlobalStyle />
    </ThemeProvider>
  );
}

export default App;
